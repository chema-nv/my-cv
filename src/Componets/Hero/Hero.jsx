
import './Hero.scss';
import '../../assets/chema.png'


const hero = ({ hero }) => {
    return (
      <div className="hero">
        <img src={require('../../assets/chema.png')} />
        <div className="card">
          <h2>
            {hero.name} {hero.adress}
          </h2>
          <p>🗺️{hero.city} </p>
          <p>🗓️{hero.birthDate}</p>
          <p>📧
            <a href={"mailto:" + hero.email}>
            noves722@gmail.com
            </a>
          </p>
          <p>📱 {hero.phone}</p>
          <p>💾<a href={hero.gitHub}>GitHub</a></p>
        </div>
      </div>
    );
  };

export default hero;