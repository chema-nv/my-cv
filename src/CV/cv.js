export const CV = {
    hero: {
      name: "José María ",
      adress: "Novés Fernádez",
      city: "Toledo",
      email: "noves722@gmail.com",
      birthDate: "7/10/1975",
      phone: "647419944",
      image: "https://i.imgur.com/ZQAkED3.png",
      gitHub: "https://github.com/tonystark",
      aboutMe: [
        {
          info: "🤖 Me gusta el deporte, la musica y salir con amigos",
        },
        {
          info: "🔩 Trabajo como tecnico de soporte a usuarios para Mapfre.",
        },
        {
          info: "🕶 Genius, billionaire, playboy, philanthropist.",
        },
        {
          info: "🦾 Soy una persona resonsable, acostumbrada a trabajar en equipo.",
        },
      ],
    },
    education: [
      {
        name: "Bootcam Full Stack Developer",
        date: "2022",
        where: "Upgrade Hub",
      },
      {
        name: "Ingenieria Tecnica Informatica",
        date: "2001",
        where: "Universidad de Castilla la Mancha",
      },
      {
        name: "Electronica Industrial",
        date: "1996",
        where: "Instituto Hermano Garate Ciudad Real",
      },
    ],
    experience: [
      {
        name: "Tecnico de soporte",
        date: "2011 – Nowadays",
        where: "INETUM",
        description:
          "Doy soporte a usuarios y resuelvo averias, tanto de software como de hardware a trabajadotes de la empresa mapfre",
      },
      {
        name: "Tecnico informatico",
        date: "2007 – 2011",
        where: "Metrolico",
        description: "Reparacion de equipos informaticos (PC´s, impresoras laser y finacieras, cajeros automaticos y dispensadores de efectivo)",
      },
      {
        name: "Tecnico informatico",
        date: "2005 – 2007",
        where: "icaldia S.L",
        description: "Reparacion de equipos informaticos (PC´s, impresoras laser y finacieras, cajeros automaticos y dispensadores de efectivo)",
      },
    ],
    languages: {
      language: "English",
      wrlevel: "Native",
      splevel: "Native",
    },
    habilities: [
      "JS",
      "HTML Y CSS",
      "ANGULAR",
      "REACT",
      "PHP",    
    ],
    volunteer: [
      {
        name: "September Foundation",
        where: "MIT",
        description:
          "The September Foundation is a program by Tony Stark to fund schools and young prodigies in their education. The foundation was named by Stark after a lyric from The Fantasticks song, 'Try to Remember,' which he heard his mother sing and play on the piano before her death.",
      },
      {
        name: "Damage Control",
        where: "U.S.A.",
        description:
          "The United States Department of Damage Control, occasionally known as the DODC, is a department of the United States of America. Initially a subsidiary of S.H.I.E.L.D., Damage Control was an organization specializing in post-battle clean-up. Following the Battle of New York, Damage Control was made into an executive branch of the United States government, and in a joint venture with Stark Industries, was tasked with acquiring alien and other dangerous artifacts along with cleaning up damages caused by enhanced individuals.",
      },
    ],
  };