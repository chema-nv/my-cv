import { useState } from 'react';
import {Hero,About,Education,Experience,More} from './Componets';
import './App.scss';
import { CV } from './CV/cv';

const {hero, education,experience, languages, habilities,volunteer } = CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);
  return (
    <div className="App">
      
      <Hero hero={hero}/>
      <About hero={hero} />
      <button className="custom-btn btn-4"onClick={() => setShowEducation(true)}>
        Education
      </button>
      <button className="custom-btn btn-4"onClick={() => setShowEducation(false)}>
        Experience
      </button>
      <div>
        {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>
	      <More
        languages={languages}
        habilities={habilities}
        volunteer={volunteer}
	      />
    </div>
  );
}

export default App;
